===========================================================================
Localization of the :math:`W^{-1,q}` norm for local a posteriori efficiency
===========================================================================

`paper-norms-nonlin-code` is hosted at https://github.com/blechta/paper-norms-nonlin-code/.
